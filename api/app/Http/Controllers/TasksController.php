<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Http\Request;

class TasksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $taskList = Task::where('deleted', 0)
            ->orderBy('created_at', 'asc')->get()->toArray();
        return response()->json($taskList);
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $r)
    {
        $taskName = !empty($r['taskName']) ? $r['taskName'] : '';
        $nameLen = strlen($taskName);
        if ($nameLen < 3 || $nameLen > 100) {
            return response()->json(['error' => 'name of the task, minimum 3 letters without spaces, max 100']);
        }

        $resp = Task::create(['name' => $r['taskName']]);

        return response()->json(["newTask" => $resp]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        if(!$task){
            return response()->json(['error' => 'task not exist!']);
        }
        return response()->json([$task]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $task = Task::find($id);
        $finished = ($task->finished === 1) ? 0 : 1;

        $task->finished = $finished;
        $result = $task->save();
        return response()->json(["result" => $result, "finished" => $finished]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);

        $task->deleted = 1;
        $result = $task->save();
        return response()->json(["result" => $result]);
    }

}
