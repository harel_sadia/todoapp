import React, { Component } from 'react';
import Task from './Task'
import axios from 'axios';
import { Link } from 'react-router-dom'



class Tasks extends Component {

    constructor(props) {
        super(props);
        axios.defaults.baseURL = 'http://localhost:8080/projects/one1/api/public/';
        axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

        console.log(props.match.params)
        let params = props.match.params;
        this.state = {
            taskList: [],
            newTaskName: '',
            error: '',
            taskLength: 0,
            taskLeft: 0
        }
        this.getTaskList(params.id);
    }
    getTaskList(id) {
        console.log(id);
        let url = 'tasks';
        if (id) {
            url += '/' + id;
        }
        var self = this;
        axios.get(url, {})
            .then(function (response) {
                self.setState({ taskList: response.data });
                self.setTodoDetails();
            });
    }

    createNewTask(taskName) {
        var self = this;
        console.log(this.state.newTaskName);
        if (!taskName) {
            return;
        }
        axios.post('tasks', { taskName: this.state.newTaskName })
            .then(function (response) {
                console.log(response.data);
                var data = response.data;

                if (data.error) {
                    self.setState({ error: data.error })
                    return;
                }
                self.setState({ error: '' })
                let newTask = response.data.newTask;
                self.setState((prevState) => {
                    prevState.taskList.push(newTask);
                    return prevState;
                });
                self.setTodoDetails();
            });
    }
    deleteTask(index, id) {
        let self = this;
        axios.delete('tasks/' + id, {})
            .then(function (response) {
                console.log(response.data.result);
                if (response.data.result) {
                    self.setState((prevState) => {
                        prevState.taskList.splice(index, 1);
                        return prevState;
                    });
                    self.setTodoDetails();
                }
            });
    }

    updateTaskData(index, finished) {
        console.log(index, finished);

        this.setState((prevState) => {
            prevState.taskList[index].finished = finished;
            return prevState;
        });
        this.setTodoDetails();
    }

    setTodoDetails() {
        this.setState({ taskLength: this.state.taskList.length })
        let taskList = this.state.taskList;
        let taskLength = 0;
        let taskLeft = 0;

        taskList.map(function (task) {
            console.log(task);
            taskLength++;
            if (task.finished === 0) {
                taskLeft++;
            }
        });
        this.setState({ taskLength: taskLength, taskLeft: taskLeft });
    }
    changeTaskView(id) {
        this.taskId = id;
        this.getTaskList(id);
    }
    render() {
        var self = this;

        const taskItems = this.state.taskList.map(function (task, index) {
            return (
                <Task
                    id={task.id}
                    key={index}
                    index={index}
                    name={task.name}
                    finished={task.finished}
                    removeMethod={self.deleteTask.bind(self, index, task.id)}
                    updateStateMethod={self.updateTaskData.bind(self)}
                    changeTaskView={self.changeTaskView.bind(self, task.id)}
                />
            );
        });

        return (
            <div id="App" className="container todolist not-done">

                <div className="row">
                    <div className="col-md-12">
                        <h1>Task list</h1>
                        
                        <div className="new-todo" >
                            <input type="text" value={this.state.newTaskName} onChange={this.updateInputValue.bind(this)} className="form-control add-todo" placeholder="New todo" min="3" max="100" />
                            <span>
                                <button onClick={this.createNewTask.bind(this)} className="btn btn-success pull-left">Add Task</button>
                            </span>
                        </div>
                        <div className="text-danger">{this.state.error}</div>
                        <hr />
                        <ul id="sortable" className="list-unstyled">
                            {taskItems}
                        </ul>
                        <hr />

                        <strong className="pull-right">
                            <Link onClick={this.changeTaskView.bind(this,null)} to={`/tasks`}><button className="btn btn-primary"> All tasks </button> </Link>
                        </strong>
                        <strong className="pull-left">
                            Total tasks: <span className="text-success"> {this.state.taskLength} </span> | 
                        </strong>
                        <strong className="pull-left">
                            Items Left to do: <span className="text-warning"> {this.state.taskLeft} </span>
                        </strong>
                    </div>
                </div>
            </div>

        );
    }
    updateInputValue(evt) {
        this.setState({
            newTaskName: evt.target.value
        });
    }
}

export default Tasks;
