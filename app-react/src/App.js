import React, { Component } from 'react';
import Tasks from './Tasks';
import { BrowserRouter as Router, Route } from "react-router-dom";


class App extends Component {
  render() {
    return (
      <Router>
          <Route path="/tasks/:id?" component={Tasks} />
      </Router>
    )
  }

}

export default App;
