import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import axios from 'axios';

class Task extends Component {

  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      checked: props.finished === 1 ? "checked" : ''
    }
  }
  taskFinished() {
    let self = this;
    let id = self.props.id;
    axios.put('tasks/' + id, {})
      .then(function (response) {
        if (response.data.result) {
          let finished = response.data.finished
          let checked = (finished === 1) ? "checked" : '';
          self.finished = finished
          self.setState({ checked: checked });
          self.props.updateStateMethod(self.props.index, finished);
        }
      });
  }
  render() {
    return (
      <li className="ui-state-default">
        <div className={this.state.checked}>
          <label className="taskName" onClick={this.taskFinished.bind(this)} > {this.props.name}</label>
          <div className="todo-buttons pull-right">
            <button onClick={this.props.removeMethod} className="btn btn-danger">Delete</button>
          </div>
          <Link onClick={this.props.changeTaskView} to={`/tasks/${this.props.id}`}>Show</Link>
        </div>
      </li>
    );
  }
}

export default Task;
