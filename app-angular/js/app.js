var TodoApp = angular.module('TodoApp', [
    'ngRoute',
]);

TodoApp.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/tasks/:task_id?', {
            templateUrl: 'views/tasks.html',
            controller: 'TasksController'
        })
        .otherwise({
            redirectTo: '/tasks'
        });

}]);