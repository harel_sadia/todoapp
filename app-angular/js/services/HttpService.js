  /* global MongoApp, MenaelUrl, google, dataObj, MenaelApp, moment, GraphService, MainScript */

  TodoApp.service('HttpService', function ($http, $timeout) {
    var API_URL = window.location.origin + '/projects/work-search-projects/one1/api/public/';
    this.getTasks = function () {
      return this.httpRequest({}, 'tasks');
    }
    this.getSingleTask = function (id) {
      return this.httpRequest({}, 'tasks/' + id);
    };
    this.createNewTask = function (taskName) {
      return this.httpRequest({
        taskName: taskName
      }, 'tasks', 'POST');
    }
    this.deleteTask = function (id) {
      return this.httpRequest({
        id: id
      }, 'tasks/' + id, 'DELETE');
    }
    this.setTaskFinished = function (id) {
      return this.httpRequest({
        id: id
      }, 'tasks/' + id, 'PUT');
    }
    /* Global http function */
    this.httpRequest = function (requestObj, url, method = 'GET') {
      console.log(method);
      var data = $http({
        url: API_URL + url,
        method: method,
        responseType: 'json',
        data: requestObj
      }).then(function (response) {
        return response.data;
      }, function (response) {
        return false;
      });
      return data;
    };

  });