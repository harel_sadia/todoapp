TodoApp.filter('taskLeft', function () {
    return function (taskList) {
        var cnt = 0;
        angular.forEach(taskList, function (task) {
            if (task.finished == 0) {
                cnt++;
            }
        });
        return cnt
    };
});