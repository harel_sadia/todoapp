TodoApp.controller('TasksController', function ($scope, $location, $routeParams, HttpService) {
    $scope.taskName = '';

    if (!$routeParams.task_id) {
        getTasks();
    } else {
        getSingleTask($routeParams.task_id);
    }

    $scope.createNewTask = function () {
        $scope.error = '';
        HttpService.createNewTask($scope.taskName).then(function (response) {
            if (!response.error) {
                $scope.taskList.push(response.newTask);
                $scope.taskName = '';
            } else {
                $scope.error = response.error;
            }
        })
    }
    $scope.deleteTask = function ($event, id, index) {
        $event.stopPropagation();
        $scope.error = '';
        HttpService.deleteTask(id).then(function (response) {
            if (response.result) {
                $scope.taskList.splice(index, 1);
            }
        });
    }
    $scope.taskFinished = function ($event, task, index) {
        $event.stopPropagation();
        $scope.error = '';
        HttpService.setTaskFinished(task.id).then(function (response) {
            console.log(response.result);
            if (response.result) {
                task.finished = response.finished;
            }
        });
    };
    $scope.goToTask = function (id) {
        $location.path('tasks/' + id);
    }

    function getTasks() {
        HttpService.getTasks().then(function (response) {
            $scope.singleTaskMode=false;
            $scope.taskList = response;
        })
    }

    function getSingleTask(id) {
        HttpService.getSingleTask(id).then(function (response) {
            $scope.singleTaskMode=true;
            if (!response.error) {
                $scope.taskList = response;
            } else {
                $scope.taskList=[];
                $scope.error = response.error
            }
        })
    }
})